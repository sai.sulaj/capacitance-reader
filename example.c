#include <EFM8LB1.h>
#include <stdio.h>

#define SYSCLK 24500000L // SYSCLK freq in Hz.
#define BAUDRATE 115200L // Baud rate of UART in bps

char _c51_external_startup (void) {
	SFRPAGE = 0x00;
	WDTCN = 0xDE;
	WDTCN = 0xAD;

	VDM0CN |= 0x80;
	RSTSRC = 0x02;

	// Use a 24.5MHz clock.
	SFRPAGE = 0x00;
	CLKSEL = 0x00;
	CLKSEL = 0x00;
	while ((CLKSEL & 0x80) == 0)
	
	P0MDOUT |= 0x10;
	XBR0 = 0x01;
	XBR1 = 0x00;
	XBR2 = 0x40;

	// Configure Uart 0.
	SCON0 = 0x10;
	CKCON0 |= 0b_0000_1000;
	TH1 = 0x100 - (SYSCLK/BAUDRATE)/2L;
	TL1 = TH1;
	TMOD &= ~0xf0;
	TMOD |= 0x20;
	TR1 = 1;
	TI = 1;

	return 0;
}

void main (void) {
	printf("Ayyyyyy lmao!\r\n");
}
